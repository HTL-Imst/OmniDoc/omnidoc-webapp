package omnidoc.business.domain.components;

import junit.framework.TestCase;
import omnidoc.business.domain.topology.ComponentType;
import omnidoc.business.domain.topology.NetworkComponent;

import java.util.Collections;

import static org.junit.Assert.assertNotEquals;

public class NetworkComponentEqualityTest extends TestCase {

    private NetworkComponent component1;
    private NetworkComponent component2;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        component1 = new NetworkComponent(ComponentType.ROUTER, "192.168.0.1", "router1", "Huawei AR3260", "123456789", Collections.singleton("FF:FF:FF:FF"));
        component2 = new NetworkComponent(ComponentType.ROUTER, "192.168.0.1", "router1", "Huawei AR3260", "123456789", Collections.singleton("FF:FF:FF:FF"));
    }

    public void testEquality() {
        assertEquals(component1, component2);
    }

    public void testEqualityWithDifferentId() {
        component1.setId(1L);
        component2.setId(2L);
        assertEquals(component1, component2);
    }

    public void testEqualityWithDifferentType() {
        component2.setType(ComponentType.CLIENT);
        assertNotEquals(component1, component2);
    }

    public void testEqualityWithDifferentHostname() {
        component2.setHostname("Different Hostname");
        assertNotEquals(component1, component2);
    }

    public void testEqualityWithDifferentIP() {
        component2.setIp("2.2.2.2");
        assertNotEquals(component1, component2);
    }


    public void testEqualityWithDifferentMax() {
        component2.setMacs(Collections.singleton("00:00:00:00"));
        assertNotEquals(component1, component2);
    }


    public void testEqualityWithDifferentModelName() {
        component2.setModelName("Different Model Name");
        assertNotEquals(component1, component2);
    }

    public void testEqualityWithDifferentSerialNumber() {
        component2.setSerialNumber("Different Serial Number");
        assertNotEquals(component1, component2);
    }
}