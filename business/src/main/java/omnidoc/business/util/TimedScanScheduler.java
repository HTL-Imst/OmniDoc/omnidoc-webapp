package omnidoc.business.util;

import omnidoc.business.domain.scan.FrequentNetworkScan;
import omnidoc.business.domain.scan.NetworkScan;
import omnidoc.business.domain.scan.ScheduledNetworkScan;
import omnidoc.business.services.scans.ScanService;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.inject.Inject;
import java.io.IOException;
import java.io.Serializable;

// TODO: Provide User Feedback if timer was not executed because the server was offline

@Singleton
public class TimedScanScheduler {

    @Resource
    private TimerService timerService;

    @Inject
    private ScanService scanService;

    public void setupSheduledScan(NetworkScan networkScan) {
        if (networkScan instanceof ScheduledNetworkScan) {
            ScheduledNetworkScan scan = (ScheduledNetworkScan) networkScan;
            TimerScanConfig scanConfig = new TimerScanConfig(networkScan.getId());
            timerService.createSingleActionTimer(scan.getScheduledDate(), new TimerConfig(scanConfig, true));
        }
    }

    // TODO: Set up Timer with ScheduleExpression for more specific scheduling

    public void setupFrequentScan(NetworkScan networkScan) {
        if (networkScan instanceof FrequentNetworkScan) {
            FrequentNetworkScan scan = (FrequentNetworkScan) networkScan;
            TimerScanConfig scanConfig = new TimerScanConfig(networkScan.getId());
            timerService.createSingleActionTimer(scan.getScheduledInterval(), new TimerConfig(scanConfig, true));
//            timerService.createCalendarTimer(new ScheduleExpression());
        }
    }

    @Timeout
    public void startScan(Timer timer) {
        TimerScanConfig scanConfig = (TimerScanConfig) timer.getInfo();
        NetworkScan networkScan = scanService.findById(scanConfig.networkScanId);
        System.out.println("Hi from TimeMethod() with " + timer.getInfo() + "scan found: " + networkScan);
        scanService.start(networkScan);
    }

    private class TimerScanConfig implements Serializable {

        private static final long serialVersionUID = 6900427634984784654L;

        private Long networkScanId;

        TimerScanConfig(Long networkScanId) {
            this.networkScanId = networkScanId;
        }

        private void writeObject(java.io.ObjectOutputStream out) throws IOException {
            out.writeLong(networkScanId);
        }

        private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
            this.networkScanId = in.readLong();
        }

    }
}
