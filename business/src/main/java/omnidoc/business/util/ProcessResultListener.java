package omnidoc.business.util;

public interface ProcessResultListener<T> {
    void onProcessStarted(T t);

    void onProcessTerminated(T t, int processResultCode);
}
