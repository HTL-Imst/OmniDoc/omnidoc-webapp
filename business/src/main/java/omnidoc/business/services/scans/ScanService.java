package omnidoc.business.services.scans;

import omnidoc.business.domain.scan.NetworkScan;

import java.util.List;

public interface ScanService {

    List<NetworkScan> findAllBySiteId(Long siteId);

    NetworkScan findById(Long id);

    void add(NetworkScan scan);

    void update(NetworkScan scan);

    void remove(NetworkScan scan);

    void start(NetworkScan scan);
}