package omnidoc.business.services.scans;

import omnidoc.business.algorithm.IScanMergeAlgorithm;
import omnidoc.business.domain.property.snmp.SCAN_ERROR;
import omnidoc.business.domain.property.snmp.SnmpProperty;
import omnidoc.business.domain.scan.FrequentNetworkScan;
import omnidoc.business.domain.scan.NetworkScan;
import omnidoc.business.domain.scan.ScanState;
import omnidoc.business.domain.scan.ScheduledNetworkScan;
import omnidoc.business.domain.topology.NetworkTopology;
import omnidoc.business.repositories.ScanRepository;
import omnidoc.business.repositories.TopologyRepository;
import omnidoc.business.util.NetworkTopologyParser;
import omnidoc.business.util.ProcessResultListener;
import omnidoc.business.util.ScanHandler;
import omnidoc.business.util.TimedScanScheduler;
import omnidoc.business.websockets.SessionHandler;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
public class ScanServiceBean implements ScanService, ProcessResultListener<NetworkScan> {

    @EJB
    private TimedScanScheduler timedScanScheduler;

    @Inject
    private ScanRepository scanRepository;

    @Inject
    private TopologyRepository topologyRepository;

    @Inject
    private ScanHandler scanHandler;

    @Inject
    private SessionHandler sessionHandler;

    @Inject
    private IScanMergeAlgorithm mergeAlgorithm;

    @Inject
    private Logger logger;

    @Override
    public List<NetworkScan> findAllBySiteId(Long siteId) {
        return scanRepository.findAllBySiteId(siteId);
    }

    @Override
    public NetworkScan findById(Long id) {
        return scanRepository.findById(id);
    }

    @Override
    public void add(NetworkScan scan) {
        scan = scanRepository.save(scan, true);

        if (scan instanceof ScheduledNetworkScan) {
            timedScanScheduler.setupSheduledScan(scan);
        } else if (scan instanceof FrequentNetworkScan) {
            timedScanScheduler.setupFrequentScan(scan);
        }
    }

    @Override
    public void update(NetworkScan scan) {
        scanRepository.update(scan);
        sessionHandler.sendUpdateScanMessage();
    }

    @Override
    public void remove(NetworkScan scan) {
        scanRepository.remove(scan);
    }

    @Override
    public void start(NetworkScan scan) {
        try {
            scanHandler.setupScan(scan);
        } catch (ClassNotFoundException e) {
            logger.log(Level.INFO, "script not found " + e.getMessage());
            e.printStackTrace();
            scan.setScanError(SCAN_ERROR.NO_SCRIPT_ERROR);
            update(scan);
        }
    }

    @Override
    public void onProcessStarted(NetworkScan scan) {
        logger.log(Level.INFO, "scan.start", scan.getName());
        scan.setState(ScanState.RUNNING);
        update(scan);
    }

    @Override
    public void onProcessTerminated(NetworkScan scan, int processResultCode) {
        logger.log(Level.INFO, "scan.stop", new String[]{scan.getName(), String.valueOf(processResultCode)});

        if (processResultCode == 0) scan.setScanError(SCAN_ERROR.SCRIPT_ERROR);

        NetworkTopology resultTopology = null;
        if (scan.getProperty() instanceof SnmpProperty) {
            SnmpProperty snmpProperty = (SnmpProperty) scan.getProperty();
            String filePath = "/scripts/" + scan.getId() + "_" + snmpProperty.getIp() + ".json";
            boolean fileExists = new File(filePath).exists();
            logger.log(Level.INFO, "Filepath: " + filePath + " exists? " + fileExists);
            try {
                resultTopology = new NetworkTopologyParser(snmpProperty.getObjectIdentifiers(), new FileReader(filePath)).parseJson();
                logger.log(Level.INFO, resultTopology.getComponents().size() + " Components found in Network");
                scan.setState(ScanState.FINISHED);
            } catch (FileNotFoundException e) {
                logger.log(Level.INFO, "file not found " + e.getMessage());
                scan.setScanError(SCAN_ERROR.NO_JSON_ERROR);
                e.printStackTrace();
            } catch (Exception e) {
                logger.log(Level.INFO, "parsing error: " + e.getMessage());
                scan.setScanError(SCAN_ERROR.PARSING_ERROR);
                e.printStackTrace();
            }
        }

        if (resultTopology == null) scan.setScanError(SCAN_ERROR.NO_RESULT_ERROR);
        scan.setResult(resultTopology);

        // Merge scan result into global Topology
        NetworkTopology currentSiteTopology = scan.getSite().getTopology();
        NetworkTopology newSiteTopology = mergeAlgorithm.setGlobalTopology(currentSiteTopology).merge(scan.getResult());
        if (currentSiteTopology != null) topologyRepository.remove(currentSiteTopology);
        scan.getSite().setTopology(newSiteTopology);

        update(scan);
    }
}
