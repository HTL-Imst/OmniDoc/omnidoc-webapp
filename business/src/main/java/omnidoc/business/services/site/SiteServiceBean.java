package omnidoc.business.services.site;

import omnidoc.business.domain.site.NetworkSite;
import omnidoc.business.repositories.SiteRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class SiteServiceBean implements SiteService {

    @Inject
    private SiteRepository siteRepository;

    public List<NetworkSite> findAll() {
        return siteRepository.findAll();
    }

    @Override
    public NetworkSite findById(Long id) {
        return siteRepository.findById(id);
    }

    public void add(NetworkSite site) {
        siteRepository.save(site, false);
    }

    @Override
    public void update(NetworkSite site) {
        siteRepository.update(site);
    }

    public void remove(NetworkSite site) {
        siteRepository.remove(site);
    }

}
