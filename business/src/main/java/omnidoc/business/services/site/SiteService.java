package omnidoc.business.services.site;

import omnidoc.business.domain.site.NetworkSite;

import java.util.List;

public interface SiteService {

    List<NetworkSite> findAll();

    NetworkSite findById(Long id);

    void add(NetworkSite site);

    void update(NetworkSite site);

    void remove(NetworkSite site);

}
