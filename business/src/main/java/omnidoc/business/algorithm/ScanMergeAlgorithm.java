package omnidoc.business.algorithm;

import omnidoc.business.domain.topology.NetworkTopology;

// TODO: Refactor packages by features and not by layers

// TODO: Implement Scan Merging Algorithm
// Global NetworkTopology (A) has priority over Topology to be merged (B).
// Use Cases:
//  --> A component in Topology B doesn't exist in Topology A: add component to Topology A
//  --> A component in Topology B already exist in Topology A: update component in Topology A
//  --> A component in Topology A doesn't exist in Topology B: component in Topology A should be marked as Offline/Not found

public class ScanMergeAlgorithm implements IScanMergeAlgorithm {

    private NetworkTopology global;

    @Override
    public IScanMergeAlgorithm setGlobalTopology(NetworkTopology global) {
        this.global = global;
        return this;
    }

    @Override
    public NetworkTopology merge(NetworkTopology topology) {
        global = topology;
        return global;
    }
}
