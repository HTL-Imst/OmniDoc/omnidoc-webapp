package omnidoc.business.algorithm;

import omnidoc.business.domain.topology.NetworkTopology;

/**
 * Algorithm that merges a {@link NetworkTopology} into a global NetworkTopology
 */
public interface IScanMergeAlgorithm {

    /**
     * Sets Global Topology
     *
     * @param global {@link NetworkTopology}
     * @return Algorithm for method chaining
     */
    IScanMergeAlgorithm setGlobalTopology(NetworkTopology global);

    /**
     * Merge passed Topology
     *
     * @param topology {@link NetworkTopology} to be merged
     * @return mreged NetworkTopology
     */
    NetworkTopology merge(NetworkTopology topology);

}
