package omnidoc.business.websockets;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

@ApplicationScoped
@ServerEndpoint("/scan")
public class ScanWebSocket {

    @Inject
    private SessionHandler sessionHandler;

    @OnOpen
    public void onOpen(Session session) {
        System.out.println("WebSocket onOpen: " + session);
        sessionHandler.addSession(session);
    }

    @OnClose
    public void onClose(Session session) {
        System.out.println("WebSocket onClose: " + session);
        sessionHandler.removeSession(session);
    }

    @OnError
    public void onError(Throwable error) {
        System.out.println("WebSocket onError: " + error);
    }

    @OnMessage
    public void onMessage(String message, Session session) {
        System.out.println("WebSocket onMessage: " + session + ", " + message);
    }

}