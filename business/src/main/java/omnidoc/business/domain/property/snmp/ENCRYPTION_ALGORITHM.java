package omnidoc.business.domain.property.snmp;

public enum ENCRYPTION_ALGORITHM {
    AES, DES
}
