package omnidoc.business.domain.property.snmp;

public enum HASH_ALGORITHM {
    MD5, SHA
}