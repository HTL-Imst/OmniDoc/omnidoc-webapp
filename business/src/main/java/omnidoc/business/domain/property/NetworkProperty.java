package omnidoc.business.domain.property;

import omnidoc.business.util.RegexUtil;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = NetworkProperty.COLUMN_TYPE)
public class NetworkProperty {

    static final String COLUMN_TYPE = "PROPERTY_TYPE";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull(message = "{ip.null}")
    @Pattern(regexp = RegexUtil.IPv4ADDRESS_PATTERN, message = "{ip.invalid}")
    private String ip;

    @Min(value = 0, message = "{subnet.min}")
    @Max(value = 30, message = "{subnet.max}")
    private int subnetBits = 0;

    public NetworkProperty() {
    }

    public NetworkProperty(String ip, int cidrBits) {
        this.ip = ip;
        this.subnetBits = cidrBits;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getSubnetBits() {
        return subnetBits;
    }

    public void setSubnetBits(int cidrBits) {
        this.subnetBits = cidrBits;
    }

}



