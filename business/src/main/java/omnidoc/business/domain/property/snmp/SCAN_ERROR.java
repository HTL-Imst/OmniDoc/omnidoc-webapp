package omnidoc.business.domain.property.snmp;

public enum SCAN_ERROR {
    NO_SCRIPT_ERROR,    // No script found to call
    NO_JSON_ERROR,      // No JSON File returned by script
    PARSING_ERROR,      // Error parsing JSON File
    NO_RESULT_ERROR,    // No results gotten from parsing JSON
    SCRIPT_ERROR        // Error returned from script
}
