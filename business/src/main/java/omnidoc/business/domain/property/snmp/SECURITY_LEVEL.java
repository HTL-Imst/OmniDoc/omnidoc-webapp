package omnidoc.business.domain.property.snmp;

public enum SECURITY_LEVEL {

    NO_AUTH_NO_PRIV("noAuthNoPriv"),
    AUTH_NO_PRIV("authNoPriv"),
    AUTH_PRIV("authPriv");

    public String name;

    SECURITY_LEVEL(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}