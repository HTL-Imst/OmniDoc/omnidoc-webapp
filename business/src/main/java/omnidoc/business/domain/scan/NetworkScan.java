package omnidoc.business.domain.scan;

import omnidoc.business.domain.property.NetworkProperty;
import omnidoc.business.domain.property.snmp.SCAN_ERROR;
import omnidoc.business.domain.site.NetworkSite;
import omnidoc.business.domain.topology.NetworkTopology;

import javax.persistence.*;
import java.util.Date;

@NamedQueries(
        @NamedQuery(
                name = NetworkScan.findBySiteId,
                query = "SELECT scan FROM NetworkScan scan WHERE scan.site = :site"
        )
)

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = NetworkScan.COLUMN_SCANTYPE)
@DiscriminatorValue(value = NetworkScan.SCAN_TYPE)
public class NetworkScan {

    public static final String findBySiteId = "NetworkScan.findAllBySiteId";

    static final String COLUMN_SCANTYPE = "Type",
            SCAN_TYPE = "N";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private NetworkSite site;

    @OneToOne(cascade = CascadeType.ALL)
    private NetworkProperty property;

    @Enumerated(EnumType.STRING)
    private ScheduleType scheduleType;

    @Enumerated(EnumType.STRING)
    private ScanState state;

    @Enumerated(EnumType.STRING)
    private SCAN_ERROR scanError;

    @OneToOne(cascade = CascadeType.ALL)
    private NetworkTopology result;

    private Date finishedDate;

    public NetworkScan() {
    }

    public NetworkScan(NetworkProperty property) {
        this.name = null;
        this.property = property;
        this.state = ScanState.PENDING;
        this.scanError = null;
    }

    public NetworkScan(String name, NetworkProperty property, ScheduleType type) {
        this.name = name;
        this.property = property;
        this.scheduleType = type;
        this.state = ScanState.PENDING;
        this.scanError = null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public NetworkSite getSite() {
        return site;
    }

    public void setSite(NetworkSite site) {
        this.site = site;
    }

    public NetworkProperty getProperty() {
        return property;
    }

    public void setProperty(NetworkProperty property) {
        this.property = property;
    }

    public ScheduleType getScheduleType() {
        return scheduleType;
    }

    public void setScheduleType(ScheduleType scheduleType) {
        this.scheduleType = scheduleType;
    }

    public ScanState getState() {
        return state;
    }

    public void setState(ScanState state) {
        if (state == ScanState.FINISHED ||
                state == ScanState.ABORTED) setFinishedDate(new Date());
        this.state = state;
    }

    public SCAN_ERROR getScanError() {
        return scanError;
    }

    public void setScanError(SCAN_ERROR scanError) {
        this.state = ScanState.ABORTED;
        this.scanError = scanError;
    }

    public NetworkTopology getResult() {
        return result;
    }

    public void setResult(NetworkTopology result) {
        this.result = result;
    }

    public Date getFinishedDate() {
        return finishedDate;
    }

    public void setFinishedDate(Date finishedDate) {
        this.finishedDate = finishedDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof NetworkScan)) return false;
        NetworkScan scan = (NetworkScan) obj;
        return id != null ? id.equals(scan.getId()) :
                (name == null || name.equals(scan.getName())) &&
                        (site == null || site.equals(scan.getSite())) &&
                        (property == null || property.equals(scan.getProperty())) &&
                        (scheduleType == null || scheduleType.equals(scan.getScheduleType())) &&
                        (state == null || state.equals(scan.getState())) &&
                        (result == null || result.equals(scan.getResult()));
    }

    @Override
    public int hashCode() {
        int hash = 53;
        return 7 * hash + (name != null ? name.hashCode() : 0) +
                7 * hash + (site != null ? site.hashCode() : 0) +
                7 * hash + (property != null ? property.hashCode() : 0) +
                7 * hash + (scheduleType != null ? scheduleType.hashCode() : 0) +
                7 * hash + (state != null ? state.hashCode() : 0) +
                7 * hash + (result != null ? result.hashCode() : 0);
    }
}
