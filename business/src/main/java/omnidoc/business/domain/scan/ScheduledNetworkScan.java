package omnidoc.business.domain.scan;

import omnidoc.business.domain.property.NetworkProperty;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@DiscriminatorValue(ScheduledNetworkScan.SCAN_TYPE)
public class ScheduledNetworkScan extends NetworkScan {

    static final String SCAN_TYPE = "S";

    @NotNull(message = "{scheduledNetworkScan.scheduledDate.null}")
    private Date scheduledDate;

    public ScheduledNetworkScan() {
    }

    public ScheduledNetworkScan(String name, NetworkProperty property, Date scheduledDate) {
        super(name, property, ScheduleType.ONCE);
        this.scheduledDate = scheduledDate;
    }

    public Date getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(Date scheduledDate) {
        this.scheduledDate = scheduledDate;
    }
}
