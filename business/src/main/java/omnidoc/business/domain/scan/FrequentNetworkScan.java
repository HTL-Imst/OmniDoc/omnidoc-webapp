package omnidoc.business.domain.scan;

import omnidoc.business.domain.property.NetworkProperty;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@DiscriminatorValue(FrequentNetworkScan.SCAN_TYPE)
public class FrequentNetworkScan extends NetworkScan {

    static final String SCAN_TYPE = "F";

    @NotNull(message = "{frequentNetworkScan.scheduledInterval.null}")
    private Date scheduledInterval;

    public FrequentNetworkScan() {
    }

    public FrequentNetworkScan(String name, NetworkProperty property, Date interval, ScheduleType type) {
        super(name, property, type);
        this.scheduledInterval = interval;
    }

    public Date getScheduledInterval() {
        return scheduledInterval;
    }

    public void setScheduledInterval(Date interval) {
        this.scheduledInterval = interval;
    }
}