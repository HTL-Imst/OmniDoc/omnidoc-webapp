package omnidoc.business.domain.scan;

public class ScanConstants {

    // Static constants
    public static final String
            JSON_ID = "ID",
            IP_ADDRESS = "IP",
            SUBNET_MASK = "SUBNET",
            SNMP_VERSION = "VERSION",
            SNMP_COMMUNITY_STRING = "COMMUNITYSTRING",
            SNMP_USERNAME = "USERNAME",
            SNMP_PASSWORD = "PASSWORD",
            SECURITY_LEVEL = "SECURITYLEVEL",
            HASH_ALGORITHM = "HASH",
            ENCRYPTION_ALGORITHM = "ENCRYPTION",
            PRIVATE_KEY = "PRIVATE KEY";


    // Dynamic oids
    public static final String
            MODEL_NAME = "MODELNAME",
            SERIALNUMBER = "SERIALNUMBER",
            MAC = "MAC",
            HOSTNAME = "HOSTNAME";

}