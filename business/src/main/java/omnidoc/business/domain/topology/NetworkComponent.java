package omnidoc.business.domain.topology;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
public class NetworkComponent implements Comparable {

    public static final String findAll = "NetworkComponent.findAll";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    private ComponentType type;

    private String ip, hostname, modelName, serialNumber;

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<String> macs;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<DynamicAttribute> dynamicAttributes;

    public NetworkComponent() {
    }

    public NetworkComponent(ComponentType type, String ip, String hostname, String modelName, String serialNumber, Set<String> macs) {
        this.type = type;
        this.hostname = hostname;
        this.ip = ip;
        this.macs = macs;
        this.modelName = modelName;
        this.serialNumber = serialNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ComponentType getType() {
        return type;
    }

    public void setType(ComponentType type) {
        this.type = type;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Set<String> getMacs() {
        return macs;
    }

    public void setMacs(Set<String> macs) {
        this.macs = macs;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public List<DynamicAttribute> getDynamicAttributes() {
        return dynamicAttributes;
    }

    public void setDynamicAttributes(List<DynamicAttribute> dynamicAttributes) {
        this.dynamicAttributes = dynamicAttributes;
    }

    @Override
    public int compareTo(Object o) {
        NetworkComponent component = (NetworkComponent) o;
        if (component != null) {
            if (this.type == component.getType()) return 0;
            if (this.type == ComponentType.ROUTER) return 1;
            if (component.getType() == ComponentType.ROUTER) return -1;
            if (this.type == ComponentType.SWITCH) return 1;
            if (component.getType() == ComponentType.SWITCH) return -1;
        }
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof NetworkComponent)) return false;
        NetworkComponent component = (NetworkComponent) obj;
        return
                (hostname == null || hostname.equals(component.getHostname())) &&
                        (modelName == null || modelName.equals(component.getModelName())) &&
                        (ip == null || ip.equals(component.getIp())) &&
                        (macs == null || macs.equals(component.getMacs())) &&
                        (serialNumber == null || serialNumber.equals(component.getSerialNumber())) &&
                        (type == null || type.equals(component.getType()));
    }

    @Override
    public int hashCode() {
        int hash = 53;
        return 7 * hash + (hostname != null ? hostname.hashCode() : 0) +
                7 * hash + (hostname != null ? hostname.hashCode() : 0) +
                7 * hash + (modelName != null ? modelName.hashCode() : 0) +
                7 * hash + (ip != null ? ip.hashCode() : 0) +
                7 * hash + (macs != null ? macs.hashCode() : 0) +
                7 * hash + (serialNumber != null ? serialNumber.hashCode() : 0) +
                7 * hash + (type != null ? type.hashCode() : 0);
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("NetworkComponent [" +
                " type=" + type +
                " hostname=" + hostname +
                " ip=" + ip +
                " modelName=" + modelName);

        s.append("mac=[");
        if (macs != null) {
            macs.forEach(mac -> {
                s.append(" ");
                s.append(mac);
            });
        }
        s.append("]");

        if (dynamicAttributes != null) {
            dynamicAttributes.forEach(dynamicAttribute -> {
                s.append(" ").append(dynamicAttribute.getName()).append("=");
                s.append("[");
                for (Attribute attribute : dynamicAttribute.getValues()) {
                    s.append(attribute.getValue());
                    s.append(",");
                }
                s.append("]");
            });
        }
        s.append("]");

        return s.toString();
    }
}
