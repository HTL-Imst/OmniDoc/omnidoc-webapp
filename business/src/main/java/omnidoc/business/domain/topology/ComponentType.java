package omnidoc.business.domain.topology;

public enum ComponentType {
    ROUTER, SWITCH, CLIENT, UNKOWN
}