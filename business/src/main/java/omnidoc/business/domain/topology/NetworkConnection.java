package omnidoc.business.domain.topology;

import javax.persistence.*;

@Entity
public class NetworkConnection {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String label;

    @OneToOne(cascade = CascadeType.ALL)
    private NetworkComponent source;

    @OneToOne(cascade = CascadeType.ALL)
    private NetworkComponent target;

    public NetworkConnection() {
    }

    public NetworkConnection(String label, NetworkComponent source, NetworkComponent target) {
        this.label = label;
        this.source = source;
        this.target = target;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public NetworkComponent getSource() {
        return source;
    }

    public void setSource(NetworkComponent source) {
        this.source = source;
    }

    public NetworkComponent getTarget() {
        return target;
    }

    public void setTarget(NetworkComponent target) {
        this.target = target;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof NetworkConnection)) return false;
        NetworkConnection component = (NetworkConnection) obj;
        return id != null ? id.equals(component.getId()) :
                (label == null || label.equals(component.getLabel())) &&
                        (source == null || source.equals(component.getTarget())) &&
                        (target == null || target.equals(component.getTarget()));
    }

    @Override
    public int hashCode() {
        int hash = 53;
        return 7 * hash + (label != null ? label.hashCode() : 0) +
                7 * hash + (source != null ? source.hashCode() : 0) +
                7 * hash + (target != null ? target.hashCode() : 0);
    }

}
