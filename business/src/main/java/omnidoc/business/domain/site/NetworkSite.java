package omnidoc.business.domain.site;

import omnidoc.business.domain.scan.NetworkScan;
import omnidoc.business.domain.topology.NetworkTopology;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NamedQueries(
        @NamedQuery(
                name = NetworkSite.findAll,
                query = "SELECT site from NetworkSite site"
        )
)

@Entity
public class NetworkSite {

    public static final String findAll = "NetworkSite.findAll";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String siteName;

    @OneToMany(mappedBy = "site", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<NetworkScan> scans;

    @OneToOne(cascade = CascadeType.ALL)
    private NetworkTopology topology;

    public NetworkSite() {
    }

    public NetworkSite(String siteName) {
        this.siteName = siteName;
        this.scans = new ArrayList<>();
    }

    public NetworkSite(String siteName, List<NetworkScan> scans) {
        this.siteName = siteName;
        this.scans = scans;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return siteName;
    }

    public void setName(String name) {
        this.siteName = name;
    }

    public List<NetworkScan> getScans() {
        return scans;
    }

    public void setScans(List<NetworkScan> scans) {
        this.scans = scans;
    }

    public void addScan(NetworkScan scan) {
        this.scans.add(scan);
    }

    public void removeScan(NetworkScan scan) {
        this.scans.remove(scan);
    }

    public NetworkTopology getTopology() {
        return topology;
    }

    public void setTopology(NetworkTopology topology) {
        this.topology = topology;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof NetworkSite)) return false;
        NetworkSite site = (NetworkSite) obj;
        return id != null ? id.equals(site.getId()) :
                (siteName == null || siteName.equals(site.getName())) &&
                        (topology == null || topology.equals(site.getTopology()));
    }

    @Override
    public int hashCode() {
        int hash = 53;
        return 7 * hash + (siteName != null ? siteName.hashCode() : 0) +
                7 * hash + (topology != null ? topology.hashCode() : 0);
    }

}
