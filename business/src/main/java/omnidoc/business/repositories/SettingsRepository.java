package omnidoc.business.repositories;

import java.util.List;

public interface SettingsRepository {

    List<String> getPanelNamesBySiteId(Long siteId);

    void addPreference(Long siteId, String panelName);

    void removePreference(Long siteId, String panelName);

}
