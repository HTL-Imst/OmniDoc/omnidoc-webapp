package omnidoc.business.repositories;

import omnidoc.business.domain.topology.NetworkTopology;

public interface TopologyRepository extends CrudRepository<NetworkTopology, Long> {
}
