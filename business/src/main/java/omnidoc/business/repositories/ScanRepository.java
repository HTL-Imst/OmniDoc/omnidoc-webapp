package omnidoc.business.repositories;

import omnidoc.business.domain.scan.NetworkScan;

import java.util.List;

public interface ScanRepository {

    List<NetworkScan> findAllBySiteId(Long siteId);

    NetworkScan findById(Long id);

    NetworkScan save(NetworkScan networkScan, boolean immediate);

    void update(NetworkScan networkScan);

    void remove(NetworkScan networkScan);
}