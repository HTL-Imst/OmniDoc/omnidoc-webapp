package omnidoc.business.repositories;

import java.util.List;

public interface CrudRepository<T, ID> {

    List<T> findAll();

    T findById(ID id);

    T save(T t);

    void update(T t);

    void remove(T t);

}
