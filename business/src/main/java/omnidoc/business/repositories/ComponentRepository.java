package omnidoc.business.repositories;

import omnidoc.business.domain.topology.NetworkComponent;

public interface ComponentRepository extends CrudRepository<NetworkComponent, Long> {
}
