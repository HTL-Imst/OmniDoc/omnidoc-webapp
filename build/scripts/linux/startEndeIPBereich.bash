#!/bin/bash


function startEndeIPBereich {
ip=$1				#IP Adresse in die einzelnen Komponenten teilen
ipBer1=${ip:0:`expr index "$ip" .`-1}			#192		=> Beispiel mit der IP: 192.168.0.0
mitte01=${ip:`expr index "$ip" .`}			#168.0.0
ipBer2=${mitte01:0:`expr index "$mitte01" .`-1}		#168
mitte02=${mitte01:`expr index "$mitte01" .`}		#0.0
ipBer3=${mitte02:0:`expr index "$mitte02" .`-1}		#0
ipBer4=${mitte02:`expr index "$mitte02" .`}		#0


###########################
sub=$2
#echo $2
#echo $sub
if [ $sub -le 30 ]
then
for i in {30..8}		#Durchlauf aller möglichen Subnetzmasken
do
if [ "$i" == "$sub" ]		#angegeben Subnetzmaske muss übereinstimmen
then
newSub=$(( 32-$i ))		#Bestimmung wie viele Bits auf 0 stehen
anzahlIPs=$((2**$newSub))	#Berechnung wie viele IPs im Netz sind + NetzID und Broadcast

fi
done
###########################

#echo ich war hier
if [ $sub -ge 24 ]								#wenn Subnetmaske kleiner gleich 24 ist
then
	if [ $anzahlIPs -le 256 ]						#Größe kontrollieren (wird hier zwar nicht unbedingt benötigt da dies durch die Subnetzmaske schon eingeschränkt wird)
	then
	local ersterHostStart="$ipBer1"".""$ipBer2"".""$ipBer3""."$(($ipBer4+1))			#erster Host zusammensetzen
	local letzterHostStart="$ipBer1"".""$ipBer2"".""$ipBer3""."$(($ipBer4+$anzahlIPs-2))		#letzter Host zusammensetzen (-2 weil mit 256 gerechnet wird)
	echo "$ersterHostStart""=""$letzterHostStart"							#Rückgabe der Wertex
	#echo $letzterHostStart
	#startPingScan $(($ipBer4+1)) $(($ipBer4+$anzahlIPs-2))				#aufruf der pingScan.bash Datei mit erstem und letztem Host
	fi

else if [ $sub -ge 16 ]
then
#Berechnung ab Sub /23 wie groß das Netzwerk ist
anzahlIPsZwi=$anzahlIPs
ipBer3_16=$ipBer3

	while [ true ]
	do
		if [ $(($anzahlIPsZwi-256)) -eq 0 ]						#sobald 0 ist wurde das Ende des des Netzes erreicht
		then
		#ipBer4=254
		break
		else
		ipBer3_16=$(($ipBer3_16+1))
		anzahlIPsZwi=$(($anzahlIPsZwi-256))
		fi
	done

	local ersterHostStart="$ipBer1"".""$ipBer2"".""$ipBer3""."$(($ipBer4+1))			#erster Host zusammensetzen
	local letzterHostStart="$ipBer1"".""$ipBer2"".""$ipBer3_16"".""254"				#letzter Host zusammensetzen
	echo "$ersterHostStart""=""$letzterHostStart"							#Rückgabe der Werte
	#echo $letzterHostStart
	


else if [ $sub -ge 8 ]
then  
#für den letzten Bereich
anzahlIPsZwi=$anzahlIPs
ipBer2_8=$ipBer2
ipBer3_8=$ipBer3

	while [ true ]
	do
		if [ $(($anzahlIPsZwi-256)) -eq 0 ]							#sobald 0 ist wurde das Ende des Netzes erreicht
		then
		break
		else
			if [ $ipBer3_8 -eq 255 ]							#Wenn das 3te voll ist wird das zweite +1 und das dritte auf 0 gesetzt
			then
			ipBer2_8=$(($ipBer2_8+1))
			ipBer3_8=0
			anzahlIPsZwi=$(($anzahlIPsZwi-256))
			
			else
			ipBer3_8=$(($ipBer3_8+1))
			anzahlIPsZwi=$(($anzahlIPsZwi-256))

			fi
		fi
	done

	local ersterHostStart="$ipBer1"".""$ipBer2"".""$ipBer3""."$(("$ipBer4+1"))			#erster Host zusammensetzen
	local letzterHostStart="$ipBer1"".""$ipBer2_8"".""255"".""254"					#letzter Host zusammensetzen
	echo "$ersterHostStart""=""$letzterHostStart"							#Rückgabe der Werte
	#echo $letzterHostStart



else
echo Ungültige Subentzmaske
fi
fi
fi
else
echo Ungültige Subentzmaske
exit
fi
}

