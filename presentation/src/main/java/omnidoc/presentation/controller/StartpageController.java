package omnidoc.presentation.controller;

import omnidoc.business.domain.site.NetworkSite;
import omnidoc.business.services.site.SiteService;
import omnidoc.presentation.manager.NavigationManager;
import omnidoc.presentation.manager.SiteManager;
import omnidoc.presentation.util.Page;
import org.primefaces.PrimeFaces;

import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.security.Principal;
import java.util.List;

@Named
@ViewScoped
public class StartpageController implements Serializable {

    private static final long serialVersionUID = -7287315117179870993L;
    private static final String AJAX_PARAM_SUCCESS = "success";

    private NetworkSite selectedNetworkSite;
    private String siteName;

    @Inject
    private SiteService siteService;

    @Inject
    private Principal principal;

    public List<NetworkSite> getSites() {
        return siteService.findAll();
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getUserName() {
        return principal.getName();
    }

    public void onSiteEntryRemoveClick(NetworkSite clickedSite) {
        selectedNetworkSite = clickedSite;
    }

    public String onSiteEntrySelect(NetworkSite clickedSite) {
        SiteManager.setCurrentSite(clickedSite);
        return NavigationManager.to(Page.DASHBOARD);
    }

    public boolean isSelected(NetworkSite networkSite) {
        if (SiteManager.getCurrentSite() == null) return false;
        return SiteManager.getCurrentSite().equals(networkSite);
    }

    /**
     * AJAX onclick event from btn-add-project
     */
    public void addSite(ActionEvent event) {
        boolean success = false;
        if (siteName != null && !siteName.isEmpty()) {
            success = true;
            siteService.add(new NetworkSite(siteName));
            siteName = "";
        }
        PrimeFaces.current().ajax().addCallbackParam(AJAX_PARAM_SUCCESS, success);
    }

    /**
     * AJAX onclick event from btn-remove-project
     */
    public void removeSelectedNetworkSite() {
        siteService.remove(selectedNetworkSite);
        if (SiteManager.isSelected(selectedNetworkSite)) SiteManager.setCurrentSite(null);
        selectedNetworkSite = null;
    }
}
