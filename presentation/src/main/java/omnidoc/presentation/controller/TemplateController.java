package omnidoc.presentation.controller;

import omnidoc.presentation.manager.MenuManager;
import omnidoc.presentation.manager.NavigationManager;
import omnidoc.presentation.manager.SiteManager;
import omnidoc.presentation.util.Page;
import org.primefaces.model.menu.MenuModel;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.security.Principal;

@Named
@SessionScoped
public class TemplateController implements Serializable {

    private static final long serialVersionUID = -7667602147240549723L;
    private static final String PARAM_PAGE = "page";

    @Inject
    private Principal user;

    @PostConstruct
    public void init() {
        MenuManager.setupMenuModel();
        SiteManager.checkSiteSelected();
    }

    public MenuModel getModel() {
        return MenuManager.getModel();
    }

    public void setCurrentMenuItemSelected() {
        FacesContext context = FacesContext.getCurrentInstance();
        String viewId = context.getViewRoot().getViewId();
        String faceletName = viewId.substring(1, viewId.indexOf('.'));
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        if (Page.DETAIL.getName().equals(faceletName) && request.getParameterMap().size() > 0) {
            faceletName = request.getParameter(PARAM_PAGE);
        }
        MenuManager.setMenuItemSelected(faceletName);
    }

    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return NavigationManager.to(Page.LOGIN);
    }

    public String checkUserLoggedIn() {
        return user.getName().equals("anonymous") ? null : NavigationManager.to(Page.STARTPAGE);
    }
}
