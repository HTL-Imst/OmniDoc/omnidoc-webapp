package omnidoc.presentation.controller;

import omnidoc.business.domain.property.NetworkProperty;
import omnidoc.business.domain.property.snmp.*;
import omnidoc.business.domain.scan.FrequentNetworkScan;
import omnidoc.business.domain.scan.NetworkScan;
import omnidoc.business.domain.scan.ScheduleType;
import omnidoc.business.domain.scan.ScheduledNetworkScan;
import omnidoc.business.domain.site.NetworkSite;
import omnidoc.business.services.scans.ScanService;
import omnidoc.business.services.snmp.ObjectIdentifierService;
import omnidoc.presentation.manager.NavigationManager;
import omnidoc.presentation.manager.SiteManager;
import omnidoc.presentation.util.Page;
import omnidoc.presentation.util.PropertyStringFormatter;
import omnidoc.presentation.util.Resources;
import omnidoc.presentation.util.validation.JSFValidationHelper;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Validator;
import javax.validation.constraints.Future;
import java.io.Serializable;
import java.util.*;

@Named
@ViewScoped
public class ScanController implements Serializable {

    private static final long serialVersionUID = 1275454840983437873L;

    private NetworkSite selectedSite;
    private List<NetworkScan> networkScans, templateScans;
    private NetworkScan selectedScanTemplate, selectedScan;

    private String name, ip, snmpCommunityString, snmpUsername, snmpPassword, privateKey;
    private int subnetBits;
    private Set<ObjectIdentifier> defaultObjectIdentifiers, objectIdentifiers;

    private String newOidLabel;
    private String newOidValue;

    private SNMP_VERSION snmpVersion;
    private SECURITY_LEVEL securityLevel;
    private HASH_ALGORITHM hashAlgorithm;
    private ENCRYPTION_ALGORITHM encryptionAlgorithm;

    @Future(message = "{scheduledNetworkScan.scheduledDate.inPast}")
    private Date scheduledDate;

    private Date scheduledInterval;

    private ScheduleType scheduleType;

    @Inject
    private ScanService scanService;

    @Inject
    private ObjectIdentifierService oidService;

    @Inject
    private FacesContext context;

    @Inject
    private Validator validator;

    @PostConstruct
    public void init() {
        selectedSite = SiteManager.getCurrentSite();
        if (selectedSite == null) return;

        defaultObjectIdentifiers = new HashSet<>(oidService.findAll());
        objectIdentifiers = new HashSet<>();
        objectIdentifiers.addAll(defaultObjectIdentifiers);

        scheduledDate = new Date();
        scheduledInterval = new Date();
        subnetBits = 24;
        snmpCommunityString = "public";

        networkScans = scanService.findAllBySiteId(selectedSite.getId());
        templateScans = getNamedScans(networkScans);
    }

    public NetworkSite getSelectedSite() {
        return selectedSite;
    }

    public NetworkScan getSelectedScanTemplate() {
        return selectedScanTemplate;
    }

    public void setSelectedScanTemplate(NetworkScan selectedScanTemplate) {
        this.selectedScanTemplate = selectedScanTemplate;
        updateScanFormFromTemplate(selectedScanTemplate);
    }

    public NetworkScan getSelectedScan() {
        return selectedScan;
    }

    public void setSelectedScan(NetworkScan selectedScan) {
        this.selectedScan = selectedScan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getSubnetBits() {
        return subnetBits;
    }

    public void setSubnetBits(Integer subnetBits) {
        this.subnetBits = subnetBits;
    }

    public Set<ObjectIdentifier> getDefaultObjectIdentifiers() {
        return defaultObjectIdentifiers;
    }

    public SNMP_VERSION getSnmpVersion() {
        return snmpVersion;
    }

    public void setSnmpVersion(SNMP_VERSION snmpVersion) {
        this.snmpVersion = snmpVersion;
    }

    public SECURITY_LEVEL getSecurityLevel() {
        return securityLevel;
    }

    public void setSecurityLevel(SECURITY_LEVEL securityLevel) {
        this.securityLevel = securityLevel;
    }

    public HASH_ALGORITHM getHashAlgorithm() {
        return hashAlgorithm;
    }

    public void setHashAlgorithm(HASH_ALGORITHM hashAlgorithm) {
        this.hashAlgorithm = hashAlgorithm;
    }

    public ENCRYPTION_ALGORITHM getEncryptionAlgorithm() {
        return encryptionAlgorithm;
    }

    public void setEncryptionAlgorithm(ENCRYPTION_ALGORITHM encryptionAlgorithm) {
        this.encryptionAlgorithm = encryptionAlgorithm;
    }

    public String getSnmpCommunityString() {
        return snmpCommunityString;
    }

    public void setSnmpCommunityString(String snmpCommunityString) {
        this.snmpCommunityString = snmpCommunityString;
    }

    public String getSnmpUsername() {
        return snmpUsername;
    }

    public void setSnmpUsername(String snmpUsername) {
        this.snmpUsername = snmpUsername;
    }

    public String getSnmpPassword() {
        return snmpPassword;
    }

    public void setSnmpPassword(String snmpPassword) {
        this.snmpPassword = snmpPassword;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public Set<ObjectIdentifier> getObjectIdentifiers() {
        return objectIdentifiers;
    }

    public void setObjectIdentifiers(Set<ObjectIdentifier> objectIdentifiers) {
        this.objectIdentifiers = objectIdentifiers;
    }

    public Date getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(Date scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    public Date getScheduledInterval() {
        return scheduledInterval;
    }

    public void setScheduledInterval(Date scheduledInterval) {
        this.scheduledInterval = scheduledInterval;
    }

    public ScheduleType[] getScheduleTypes() {
        return ScheduleType.values();
    }

    public ScheduleType getScheduleType() {
        return scheduleType;
    }

    public String getNewOidLabel() {
        return newOidLabel;
    }

    public void setNewOidLabel(String newOidLabel) {
        this.newOidLabel = newOidLabel;
    }

    public String getNewOidValue() {
        return newOidValue;
    }

    public void setNewOidValue(String newOidValue) {
        this.newOidValue = newOidValue;
    }

    public void setScheduleType(ScheduleType scheduleType) {
        this.scheduledDate = new Date();
        this.scheduledInterval = new Date();
        this.scheduleType = scheduleType;
    }

    public List<NetworkScan> getNetworkScans() {
        return networkScans;
    }

    public List<NetworkScan> getNamedNetworkScans() {
        return templateScans;
    }

    private List<NetworkScan> getNamedScans(List<NetworkScan> networkScans) {
        List<NetworkScan> namedScans = new ArrayList<>();
        networkScans.forEach(scan -> {
            if (scan.getName() != null && !scan.getName().isEmpty()) namedScans.add(scan);
        });
        return namedScans;
    }

    private void updateScanFormFromTemplate(NetworkScan selectedNetworkScan) {
        if (selectedNetworkScan != null) {

            NetworkProperty property = selectedNetworkScan.getProperty();
            ip = property.getIp();
            subnetBits = property.getSubnetBits();

            if (property instanceof SnmpProperty) {
                SnmpProperty snmpProperty = (SnmpProperty) property;

                Set<ObjectIdentifier> identifiers = snmpProperty.getObjectIdentifiers();
                objectIdentifiers = new HashSet<>();
                objectIdentifiers.addAll(identifiers);

                this.snmpVersion = snmpProperty.getVersion();
                if (snmpVersion != null) {
                    switch (snmpProperty.getVersion()) {
                        case VERSION_1:
                            snmpCommunityString = snmpProperty.getCommunityString();
                            break;
                        case VERSION_2:
                            break;
                        case VERSION_3:
                            SnmpCredentialProperty credentialScan = (SnmpCredentialProperty) property;
                            snmpUsername = credentialScan.getUsername();
                            snmpPassword = credentialScan.getPassword();
                            securityLevel = credentialScan.getSecurityLevel();
                            hashAlgorithm = credentialScan.getHashAlgorithm();
                            encryptionAlgorithm = credentialScan.getEncryptionAlgorithm();
                            break;
                    }
                }
            }
        }
    }

    public SNMP_VERSION[] getSnmpVersions() {
        return SNMP_VERSION.values();
    }

    public SECURITY_LEVEL[] getSecurityLevels() {
        return SECURITY_LEVEL.values();
    }

    public HASH_ALGORITHM[] getHashAlgorithms() {
        return HASH_ALGORITHM.values();
    }

    public ENCRYPTION_ALGORITHM[] getEncryptionAlgorithms() {
        return ENCRYPTION_ALGORITHM.values();
    }

    public String checkSiteSelected() {
        return SiteManager.getCurrentSite() == null ?
                NavigationManager.to(Page.STARTPAGE) : null;
    }

    public void addScan() {
        NetworkProperty property = createProperty();
        NetworkScan scan = createScan(property);
        scan.setSite(selectedSite);

        JSFValidationHelper<NetworkProperty> propertyValidator = new JSFValidationHelper<>(context, validator);
        JSFValidationHelper<NetworkScan> scanValidator = new JSFValidationHelper<>(context, validator);

        if (propertyValidator.validate(property) && scanValidator.validate(scan)) {
            scanService.add(scan);
            networkScans.add(scan);
        } else {
            context.getPartialViewContext().getRenderIds().add("scan-form");
        }
    }

    private NetworkScan createScan(NetworkProperty property) {
        if (scheduleType != null) {
            switch (scheduleType) {
                case ONCE:
                    return new ScheduledNetworkScan(name, property, scheduledDate);
                case DAILY:
                case WEEKLY:
                case CUSTOM:
                    return new FrequentNetworkScan(name, property, scheduledInterval, scheduleType);
                default:
                    return new NetworkScan(name, property, scheduleType);
            }
        } else {
            return new NetworkScan(name, property, ScheduleType.MANUAL);
        }
    }

    private NetworkProperty createProperty() {
        switch (snmpVersion) {
            case VERSION_1:
            case VERSION_2:
                return new SnmpProperty(ip, subnetBits, objectIdentifiers, snmpVersion, snmpCommunityString);
            case VERSION_3:
                if (encryptionAlgorithm != null)
                    return new SnmpCredentialProperty(ip, subnetBits, objectIdentifiers, snmpCommunityString, snmpUsername, snmpPassword, hashAlgorithm, encryptionAlgorithm, privateKey);
                if (hashAlgorithm != null)
                    return new SnmpCredentialProperty(ip, subnetBits, objectIdentifiers, snmpCommunityString, snmpUsername, snmpPassword, hashAlgorithm);
                return new SnmpCredentialProperty(ip, subnetBits, objectIdentifiers, snmpCommunityString, snmpUsername, snmpPassword);
            default:
                return new NetworkProperty(ip, subnetBits);
        }
    }

    public void startScan(NetworkScan scan) {
        scanService.start(scan);
    }

    public void removeScan(NetworkScan scan) {
        scanService.remove(scan);
        networkScans.remove(scan);
    }

    public String getScheduleTypeText(NetworkScan scan) {
        if (scan == null) return null;
        Date date = null;
        if (scan instanceof ScheduledNetworkScan) {
            date = ((ScheduledNetworkScan) scan).getScheduledDate();
        }
        if (scan instanceof FrequentNetworkScan) {
            date = ((FrequentNetworkScan) scan).getScheduledInterval();
        }
        return PropertyStringFormatter.fromScheduleType(scan.getScheduleType(), date);
    }

    public String getScanStateText(NetworkScan scan) {
        if (scan == null) return null;
        ResourceBundle bundle = Resources.getMessageBundle(Resources.scanBundle);
        String messageKey = "scan.state." + scan.getState().name();
        return bundle.getString(messageKey);
    }

    public void addOid() {
        if (newOidLabel.isEmpty() || newOidValue.isEmpty()) return;
        ObjectIdentifier objectIdentifier = new ObjectIdentifier(newOidLabel, newOidValue);
        oidService.add(objectIdentifier);
        objectIdentifiers.add(objectIdentifier);
        defaultObjectIdentifiers.add(objectIdentifier);
        newOidLabel = "";
        newOidValue = "";
    }
}
