package omnidoc.presentation.controller;

import omnidoc.presentation.manager.NavigationManager;
import omnidoc.presentation.model.panel.PanelType;
import omnidoc.presentation.util.Page;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class DetailController implements Serializable {

    private static final long serialVersionUID = -8180717019323789254L;
    private String panelType;

    public String getPanelType() {
        return panelType;
    }

    public void setPanelType(String panelType) {
        this.panelType = panelType;
    }

    public String getPagePath() {
        PanelType panel = PanelType.fromName(panelType);
        return panel != null ? panel.getPath() : "";
    }

    public String checkPage() {
        if (panelType == null || PanelType.fromName(this.panelType) == null) return NavigationManager.to(Page.STARTPAGE);
        return null;
    }
}
