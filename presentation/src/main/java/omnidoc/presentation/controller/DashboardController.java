package omnidoc.presentation.controller;

import omnidoc.business.domain.site.NetworkSite;
import omnidoc.business.domain.topology.NetworkTopology;
import omnidoc.business.services.settings.ISettingsService;
import omnidoc.business.services.site.SiteService;
import omnidoc.presentation.manager.NavigationManager;
import omnidoc.presentation.manager.SiteManager;
import omnidoc.presentation.model.panel.DashboardPanel;
import omnidoc.presentation.model.panel.PanelProducer;
import omnidoc.presentation.model.panel.PanelType;
import omnidoc.presentation.model.preference.DashboardPreference;
import omnidoc.presentation.util.Page;
import omnidoc.presentation.util.Resources;
import org.primefaces.component.panel.Panel;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.DashboardReorderEvent;
import org.primefaces.model.DashboardColumn;
import org.primefaces.model.DashboardModel;
import org.primefaces.model.DefaultDashboardColumn;
import org.primefaces.model.DefaultDashboardModel;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.*;

/**
 * TODO: Diagrams should be editable (add router/switch/client and connections)
 * - auto load rest of info from db if new device is created with existing info
 * - maybe panel should have small menu to add devices
 * - or only detail page can be used to edit info
 * - diagram could be updateable with new scan results (git like diff)
 * - mindmaps should be used for grouping items like ips in a subnet
 * - organigram could be used
 * - for locations/projects and their subgroups
 * - for user rights control
 */

@Named
@ViewScoped
public class DashboardController implements Serializable {

    private static final long serialVersionUID = 248565168871629523L;

    private NetworkSite selectedSite;
    private DashboardModel dashboard;
    private List<DashboardPanel> panels;
    private ResourceBundle bundle;

    @Inject
    private SiteService siteService;

    @Inject
    private ISettingsService settingsService;

    @PostConstruct
    public void init() {
        selectedSite = SiteManager.getCurrentSite();
        if (selectedSite == null) return;
        selectedSite = siteService.findById(SiteManager.getCurrentSite().getId());

        bundle = Resources.getMessageBundle(Resources.dashboardBundle);

        if (selectedSite != null) {
            SiteManager.setCurrentSite(selectedSite);
            if (selectedSite.getTopology() == null ||
                    selectedSite.getTopology().getComponents().isEmpty()) {
                sendDashboardIsEmptyMessage();
            }
        }

        panels = setupPanels(selectedSite);
        populateDashboard();
    }

    private void sendDashboardIsEmptyMessage() {
        FacesMessage message = new FacesMessage(
                bundle.getString("dashboardIsEmpty"),
                bundle.getString("dashboardIsEmptyTip")
        );
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public DashboardModel getDashboard() {
        return dashboard;
    }

    public List<DashboardPanel> getPanels() {
        return panels;
    }

    public NetworkSite getSelectedSite() {
        return SiteManager.getCurrentSite();
    }

    /**
     * Gets called on {@link Page#DASHBOARD} load.
     * If no {@link NetworkSite} is selected by the User, the page will be redirected to {@link Page#STARTPAGE}
     *
     * @return Redirection Path to {@link Page#STARTPAGE} or null
     */
    public String checkForRedirection() {
        return SiteManager.getCurrentSite() != null ? null : NavigationManager.to(Page.STARTPAGE);
    }

    private List<DashboardPanel> setupPanels(NetworkSite selectedSite) {
        if (selectedSite == null) return new ArrayList<>();
        List<String> panelNames = settingsService.findAllPanelsBySite(selectedSite);
        if (panelNames.isEmpty()) panelNames = createDefaultDashboardLayout(selectedSite.getTopology());
        DashboardPreference preference = DashboardPreference.fromPanelNames(panelNames);
        return preference.getPanels();
    }

    private List<String> createDefaultDashboardLayout(NetworkTopology topology) {
        List<String> panelNames = new ArrayList<>();
        if (topology == null) return panelNames;
        panelNames.add(PanelType.MINDMAP.getName());
        panelNames.add(PanelType.COMPONENTS.getName());
        panelNames.add(PanelType.DONUT_CHART.getName());
        return panelNames;
    }

    /**
     * Populates Dashboard from {@link #panels} List
     */
    private void populateDashboard() {
        dashboard = new DefaultDashboardModel();

        DashboardColumn column1 = new DefaultDashboardColumn();
        DashboardColumn column2 = new DefaultDashboardColumn();
        DashboardColumn column3 = new DefaultDashboardColumn();

        if (panels != null && !panels.isEmpty()) {
            for (DashboardPanel panel : panels) {
                column1.addWidget(panel.getId());
            }
        }

        dashboard.addColumn(column1);
        dashboard.addColumn(column2);
        dashboard.addColumn(column3);
    }

    /**
     * Gets all availbale Panel Types for Topology.
     *
     * @return Array of PanelTypes
     */
    public PanelType[] getPanelTypes() {
        PanelType[] panels = PanelType.values();
        NetworkTopology topology = selectedSite.getTopology();
        if (topology == null || topology.getComponents().isEmpty()) {
            for (PanelType panel : panels) {
                if (panel == PanelType.DONUT_CHART ||
                        panel == PanelType.MINDMAP ||
                        panel == PanelType.COMPONENTS ||
                        panel == PanelType.EDITABLE_DIAGRAM) {
                    panel.setEnabled(false);
                }
            }
        }
        return panels;
    }

    /**
     * Prepares Types of Panels for facelet
     *
     * @return List of {@link DashboardPanel}
     */
    public List<DashboardPanel> getDashboardPanelTypes() {
        String messageKey = "panel.";
        String header;
        List<DashboardPanel> panels = new ArrayList<>();
        for (PanelType panelType : PanelType.values()) {
            if (bundle.containsKey(messageKey + panelType.getName())) {
                header = bundle.getString(messageKey + panelType.getName());
            } else {
                header = bundle.getString(messageKey + "default");
            }
            panels.add(new DashboardPanel(panelType.getName(), header, panelType));
        }
        return panels;
    }

    /**
     * Adds Panel from facelet
     *
     * @param panelType Type of {@link DashboardPanel}
     */
    public void addPanel(String panelType) {
        if (panelType == null || panelType.isEmpty()) return;

        DashboardPanel dashboardPanel = PanelProducer.fromName(panelType);
        if (dashboardPanel == null || panels.contains(dashboardPanel)) return;

        panels.add(dashboardPanel);
        settingsService.addPreference(SiteManager.getCurrentSite(), dashboardPanel.getType().getName());
        populateDashboard();
    }

    /**
     * Removes Panel from facelet
     *
     * @param dashboardPanel Panel to remove
     */
    private void removePanel(DashboardPanel dashboardPanel) {
        panels.remove(dashboardPanel);
        settingsService.removePreference(SiteManager.getCurrentSite(), dashboardPanel.getType().getName());
        populateDashboard();
    }

    /**
     * AJAX reorder event
     *
     * @param event from dashboard
     */
    public void handleReorder(DashboardReorderEvent event) {

        if (event.getWidgetId() == null || event.getWidgetId().isEmpty()) return;

        DashboardPanel panel = PanelProducer.fromName(event.getWidgetId());
        if (panel != null) {
            panels.add(event.getItemIndex(), panel);
        } else {
            // Positioning widget in dashboard
            Iterator<DashboardPanel> it = panels.iterator();
            while (it.hasNext()) {
                panel = it.next();
                if (panel.getId().equals(event.getWidgetId())) {
                    it.remove();
                    panels.add(event.getItemIndex(), panel);
                    break;
                }
            }
        }

        FacesMessage message = new FacesMessage();
        message.setSeverity(FacesMessage.SEVERITY_INFO);
        message.setSummary("Reordered: " + event.getWidgetId());
        message.setDetail("Item index: " + event.getItemIndex() + ", Column index: " + event.getColumnIndex() + ", Sender index: " + event.getSenderColumnIndex());
    }

    /**
     * AJAX close event from dashboard
     *
     * @param event from panel
     */
    public void handlePanelClose(CloseEvent event) {
        Panel panelToClose = (Panel) event.getSource();
        Iterator<DashboardPanel> it = panels.iterator();
        while (it.hasNext()) {
            DashboardPanel panel = it.next();
            if (panel.getId().equals(panelToClose.getId())) {
                it.remove();
                return;
            }
        }
    }

    /**
     * AJAX Remote Command called in onDrop Event fom droppable
     */
    public void onPanelDrop() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        addPanel(params.get("panelType"));
    }
}
