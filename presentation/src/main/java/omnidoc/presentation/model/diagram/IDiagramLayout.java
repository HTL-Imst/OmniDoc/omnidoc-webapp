package omnidoc.presentation.model.diagram;

/**
 * This specifies the layout of the components and the elements of the diagram
 */
public interface IDiagramLayout {

    void applyLayout();

}
