package omnidoc.presentation.model.diagram;

import omnidoc.business.domain.topology.NetworkComponent;
import omnidoc.business.domain.topology.NetworkTopology;
import omnidoc.presentation.manager.SiteManager;
import omnidoc.presentation.util.Resources;
import org.primefaces.PrimeFaces;
import org.primefaces.event.diagram.ConnectEvent;
import org.primefaces.event.diagram.ConnectionChangeEvent;
import org.primefaces.event.diagram.DisconnectEvent;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.overlay.ArrowOverlay;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@RequestScoped
public class EditableDiagramView implements Serializable {

    private static final long serialVersionUID = -6012752737484038801L;

    private DefaultDiagramModel model;

    private boolean suspendEvent;

    @PostConstruct
    public void init() {
        setupDiagram();
        NetworkTopology topology = SiteManager.getCurrentSite().getTopology();
        if (topology != null) {
            DefaultDiagramLayout layout = new DefaultDiagramLayout(topology);
            layout.applyLayout();
            populateDiagram(
                    layout.getElements(),
                    layout.getConnections()
            );
        }
    }

    public DiagramModel getModel() {
        return model;
    }

    /**
     * Consctruct Diagram and set default connector and overlays
     */
    private void setupDiagram() {
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.getDefaultConnectionOverlays().add(new ArrowOverlay(20, 20, 1, 1));
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:' #" + Resources.Colors.primaryBlue + "', lineWidth:3}");
        connector.setHoverPaintStyle("{strokeStyle:' #" + Resources.Colors.accentYellowDark + "'}");
        model.setDefaultConnector(connector);
    }

    /**
     * Add elements and connections to diagram
     *
     * @param elements    to be added to diagram
     * @param connections to be added to diagram
     */
    private void populateDiagram(List<Element> elements, List<Connection> connections) {
        elements.forEach(element -> model.addElement(element));
        connections.forEach(connection -> model.connect(connection));
    }

    /**
     * Used in detail.xhtml to get icon according to component type displayed
     *
     * @param component displayed in diagram
     * @return path to image
     */
    public String getImagePath(NetworkComponent component) {
        switch (component.getType()) {
            case ROUTER:
                return Resources.Images.ROUTER_SMALL;
            case SWITCH:
                return Resources.Images.SWITCH_SMALL;
            case CLIENT:
                return Resources.Images.WORKSTATION_SMALL;
            default:
                return "";
        }
    }

    /**
     * AJAX connect event from diagram
     *
     * @param event ConnectEvent
     */
    public void onConnect(ConnectEvent event) {
        if (!suspendEvent) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Connected",
                    "From " + event.getSourceElement().getData() + " To " + event.getTargetElement().getData());

            FacesContext.getCurrentInstance().addMessage(null, msg);

            PrimeFaces.current().ajax().update("form:msgs");
        } else {
            suspendEvent = false;
        }
    }

    /**
     * AJAX disconnect event from diagram
     *
     * @param event DisconnectEvent
     */
    public void onDisconnect(DisconnectEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Disconnected",
                "From " + event.getSourceElement().getData() + " To " + event.getTargetElement().getData());

        FacesContext.getCurrentInstance().addMessage(null, msg);

        PrimeFaces.current().ajax().update("form:msgs");
    }

    /**
     * AJAX connection event from diagram
     *
     * @param event ConnectionChangeEvent
     */
    public void onConnectionChange(ConnectionChangeEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Connection Changed",
                "Original Source:" + event.getOriginalSourceElement().getData() +
                        ", New Source: " + event.getNewSourceElement().getData() +
                        ", Original Target: " + event.getOriginalTargetElement().getData() +
                        ", New Target: " + event.getNewTargetElement().getData());

        FacesContext.getCurrentInstance().addMessage(null, msg);

        PrimeFaces.current().ajax().update("form:msgs");
        suspendEvent = true;
    }

}
