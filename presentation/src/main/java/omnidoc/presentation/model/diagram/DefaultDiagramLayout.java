package omnidoc.presentation.model.diagram;

import omnidoc.business.domain.topology.ComponentType;
import omnidoc.business.domain.topology.NetworkComponent;
import omnidoc.business.domain.topology.NetworkConnection;
import omnidoc.business.domain.topology.NetworkTopology;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class DefaultDiagramLayout implements IDiagramLayout {

    private List<Element> elements;

    private List<Connection> connections;

    private int routerCounter = 0,
            switchCounter = 0,
            clientCounter = 0;

    DefaultDiagramLayout(NetworkTopology topology) {
        elements = convertComponents(topology.getComponents());
        connections = convertConnections(elements, topology.getConnections());
    }

    public List<Element> getElements() {
        return elements;
    }

    public List<Connection> getConnections() {
        return connections;
    }

    @Override
    public void applyLayout() {
        sortElements();
        calculatePositions();
    }

    /**
     * Convert NetworkComponents into usable Element Objects
     *
     * @param networkComponents to convert
     * @return Element list used in diagram
     */
    private List<Element> convertComponents(Set<NetworkComponent> networkComponents) {
        List<Element> elements = new ArrayList<>();
        for (NetworkComponent component : networkComponents) {
            switch (component.getType()) {
                case ROUTER:
                    routerCounter++;
                    break;
                case SWITCH:
                    switchCounter++;
                    break;
                case CLIENT:
                    clientCounter++;
                    break;
                default:
                    elements.add(new Element());
            }
            Element element = new Element(component);
            EndPoint endPoint = new DotEndPoint(EndPointAnchor.AUTO_DEFAULT);
            endPoint.setScope("network");
            endPoint.setStyle("{fillStyle:'#98AFC7'}");
            endPoint.setHoverStyle("{fillStyle:'#5C738B'}");
            element.addEndPoint(endPoint);
            elements.add(element);
        }
        return elements;
    }

    /**
     * Convert NetworkConnections into usable Connection Objects
     *
     * @param elements           list of elements used in diagram
     * @param networkConnections to be converted
     * @return connection list
     */
    private List<Connection> convertConnections(List<Element> elements, Set<NetworkConnection> networkConnections) {
        List<Connection> connections = new ArrayList<>();
        for (NetworkConnection networkConnection : networkConnections) {
            EndPoint source = null;
            EndPoint target = null;
            for (Element element : elements) {
                NetworkComponent component = (NetworkComponent) element.getData();
                Long connectionSourceId = networkConnection.getSource().getId();
                Long connectionTargetId = networkConnection.getTarget().getId();
                if (component.getId().equals(connectionSourceId)) {
                    source = element.getEndPoints().get(0);
                }
                if (component.getId().equals(connectionTargetId)) {
                    target = element.getEndPoints().get(0);
                }
            }
            Connection connection = new Connection(source, target, new StraightConnector());
            connections.add(connection);
        }
        return connections;
    }

    /**
     * Order the elements according to their NetworkComponent Type
     * Type.Router > Type.Switch > Type.Client
     */
    private void sortElements() {
        elements.sort((o1, o2) -> {
            NetworkComponent component1 = (NetworkComponent) o1.getData();
            NetworkComponent component2 = (NetworkComponent) o2.getData();
            if (component1 != null && component2 != null) {
                if (component1.getType() == component2.getType()) return 0;
                if (component1.getType() == ComponentType.ROUTER) return 1;
                if (component2.getType() == ComponentType.ROUTER) return -1;
                if (component1.getType() == ComponentType.SWITCH) return 1;
                if (component2.getType() == ComponentType.SWITCH) return -1;
            }
            return 0;
        });
    }

    /**
     * Calculate the positions of the elements according to their hierarchy.
     */
    private void calculatePositions() {
        for (Element element : elements) {
            int posX = 10;
            int posY = 10;

            if (element.getData() != null) {
                NetworkComponent component = (NetworkComponent) element.getData();

                switch (component.getType()) {
                    case ROUTER:
                        posX = posX + (--routerCounter * 15);
                        break;
                    case SWITCH:
                        posX = posX + (--switchCounter * 15);
                        posY = posY + 15;
                        break;
                    case CLIENT:
                        posX = posX + (--clientCounter * 15);
                        posY = posY + 30;
                        break;
                }
            }
            element.setX(posX + "em");
            element.setY(posY + "em");
        }
    }
}
