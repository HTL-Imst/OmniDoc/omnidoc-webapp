package omnidoc.presentation.model.diagram;

import omnidoc.business.domain.site.NetworkSite;
import omnidoc.business.domain.topology.ComponentType;
import omnidoc.business.domain.topology.NetworkComponent;
import omnidoc.business.services.site.SiteService;
import omnidoc.presentation.manager.SiteManager;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.mindmap.DefaultMindmapNode;
import org.primefaces.model.mindmap.MindmapNode;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class MindmapView implements Serializable {

    private static final long serialVersionUID = -2809769319088602839L;

    private static final String
            NODE_TYPE_ROUTER = "ROUTER",
            NODE_TYPE_SWITCH = "SWITCH",
            NODE_TYPE_CLIENT = "CLIENT",
            NODE_TYPE_IPS = "IPs";

    private NetworkSite site;

    private MindmapNode root, selectedNode;

    @Inject
    private SiteService siteService;

    public MindmapView() {
    }

    @PostConstruct
    public void init() {
        site = SiteManager.getCurrentSite();
        populateDiagram(site);
    }

    private void populateDiagram(NetworkSite site) {

        root = createPrimaryNode(site.getName());

        MindmapNode routerComponents = createNode(NODE_TYPE_ROUTER, true);
        MindmapNode switchComponents = createNode(NODE_TYPE_SWITCH, true);
        MindmapNode clientComponents = createNode(NODE_TYPE_CLIENT, true);
        MindmapNode ips = createNode(NODE_TYPE_IPS, true);

        root.addNode(routerComponents);
        root.addNode(switchComponents);
        root.addNode(clientComponents);
        root.addNode(ips);
    }

    public MindmapNode getRoot() {
        return root;
    }

    public MindmapNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(MindmapNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public void onNodeSelect(SelectEvent event) {
        MindmapNode node = (MindmapNode) event.getObject();
        if (!node.getChildren().isEmpty()) return;

        //populate if not already loaded
        Object o = node.getLabel();
        for (NetworkComponent c : site.getTopology().getComponents()) {
            if (isType(o, c, "ROUTER", ComponentType.ROUTER) ||
                    isType(o, c, "SWITCH", ComponentType.SWITCH) ||
                    isType(o, c, "CLIENT", ComponentType.CLIENT)) {
                node.addNode(createChild(c.getHostname(), c));
            } else if (o.equals("IPs")) {
                node.addNode(createChild(c.getIp(), c));
            }
        }
    }


    private boolean isType(Object label, NetworkComponent component, String name, ComponentType type) {
        return label.equals(name) && component.getType() == type;
    }

    private DefaultMindmapNode createChild(String name, Object data) {
        return new DefaultMindmapNode(name, data, "82c542", true);
    }

    public void onNodeDblselect(SelectEvent event) {
        this.selectedNode = (MindmapNode) event.getObject();
    }

    private DefaultMindmapNode createPrimaryNode(String label) {
        return new DefaultMindmapNode(label, null, "FFCC00", true);
    }

    private DefaultMindmapNode createNode(String label, boolean selectable) {
        return new DefaultMindmapNode(label, null, "6e9ebf", selectable);
    }
}