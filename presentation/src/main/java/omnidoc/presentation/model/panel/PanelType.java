package omnidoc.presentation.model.panel;

import omnidoc.presentation.util.Page;

public enum PanelType {

    HELLO("hello", "WEB-INF/includes/hello.xhtml", "fa-users"),
    COMPONENTS("components", "WEB-INF/includes/components.xhtml", "fa-list-alt"),
    SIMPLE_DIAGRAM(Page.SIMPLE_DIAGRAM.getName(), "WEB-INF/includes/simple-diagram.xhtml", "fa-sitemap"),
    EDITABLE_DIAGRAM(Page.EDITABLE_DIAGRAM.getName(), "WEB-INF/includes/editable-diagram.xhtml", "fa-sitemap"),
    MINDMAP(Page.MINDMAP.getName(), "WEB-INF/includes/mindmap.xhtml", "fa-sitemap"),
    ORGANIGRAM(Page.ORGANIGRAM.getName(), "WEB-INF/includes/organigram.xhtml", "fa-sitemap"),
    DONUT_CHART("donut-chart", "WEB-INF/includes/donut-chart.xhtml", "fa-pie-chart");


    private String name, path, css;
    private boolean enabled;

    PanelType(String name, String path, String css) {
        this.name = name;
        this.path = path;
        this.css = css;
        this.enabled = true;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public String getCss() {
        return css;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public static PanelType fromName(String includeType) {
        if (includeType != null && !includeType.isEmpty()) {
            for (PanelType type : PanelType.values()) {
                if (type.getName().toLowerCase().equals(includeType.toLowerCase())) return type;
            }
        }
        return null;
    }
}