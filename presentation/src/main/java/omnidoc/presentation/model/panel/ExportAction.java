package omnidoc.presentation.model.panel;

public class ExportAction extends PanelAction {

    public ExportAction(String onClick) {
        setLink("#");
        setStyleClass("fa fa-download");
        setOnClick(onClick);
    }
}
