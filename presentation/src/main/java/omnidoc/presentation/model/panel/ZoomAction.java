package omnidoc.presentation.model.panel;

import omnidoc.presentation.manager.NavigationManager;
import omnidoc.presentation.util.Page;

public class ZoomAction extends PanelAction {

    private Page page;

    ZoomAction(Page page) {
        this.page = page;
        setStyleClass("ui-icon-zoomin");
        setOnClick("");
    }

    @Override
    public String getLink() {
        return NavigationManager.to(page);
    }
}
