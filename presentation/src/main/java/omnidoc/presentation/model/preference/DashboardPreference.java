package omnidoc.presentation.model.preference;

import omnidoc.presentation.model.panel.DashboardPanel;
import omnidoc.presentation.model.panel.PanelProducer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DashboardPreference {
    private List<DashboardPanel> panels;

    public static DashboardPreference fromPanelNames(List<String> panelNames) {
        return new DashboardPreference(panelNames
                .stream()
                .map(PanelProducer::fromName)
                .collect(Collectors.toList()));
    }

    public DashboardPreference() {
        panels = new ArrayList<>();
    }

    public DashboardPreference(List<DashboardPanel> panels) {
        this.panels = panels;
    }

    public List<DashboardPanel> getPanels() {
        return panels;
    }

    public void setPanels(List<DashboardPanel> panels) {
        this.panels = panels;
    }

    public void addPanel(DashboardPanel panel) {
        panels.add(panel);
    }

    public void removePanel(DashboardPanel panel) {
        panels.remove(panel);
    }
}
