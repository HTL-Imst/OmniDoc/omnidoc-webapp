package omnidoc.presentation.model.components;


import omnidoc.business.domain.site.NetworkSite;
import omnidoc.business.domain.topology.ComponentType;
import omnidoc.business.domain.topology.NetworkComponent;
import omnidoc.business.services.components.ComponentService;
import omnidoc.business.services.site.SiteService;
import omnidoc.presentation.exporter.ExportType;
import omnidoc.presentation.manager.SiteManager;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Named
@ViewScoped
public class ComponentView implements Serializable {

    private static final long serialVersionUID = -3003122957621343180L;

    private List<NetworkComponent> componentList;
    private NetworkComponent selectedComponent, addComponent;
    private ExportType exportType;
    private String fileName;

    @Inject
    private SiteService siteService;

    @Inject
    private ComponentService componentService;

    @PostConstruct
    public void init() {
        NetworkSite selectedSite = siteService.findById(SiteManager.getCurrentSite().getId());
        SiteManager.setCurrentSite(selectedSite);

        fileName = selectedSite.getName() + "-" + new SimpleDateFormat("YYYY-MM-DD").format(new Date());

        if (selectedSite.getTopology() == null) return;
        componentList = new ArrayList<>();
        componentList.addAll(selectedSite.getTopology().getComponents());

        addComponent = new NetworkComponent();
    }

    public List<NetworkComponent> getComponentList() {
        return componentList;
    }

    public void setComponentList(List<NetworkComponent> componentList) {
        this.componentList = componentList;
    }

    public NetworkComponent getSelectedComponent() {
        return selectedComponent;
    }

    public void setSelectedComponent(NetworkComponent selectedComponent) {
        this.selectedComponent = selectedComponent;
    }

    public NetworkComponent getAddComponent() {
        return addComponent;
    }

    public void setAddComponent(NetworkComponent addComponent) {
        this.addComponent = addComponent;
    }

    public ExportType getExportType() {
        return exportType;
    }

    public void setExportType(ExportType exportType) {
        this.exportType = exportType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public ComponentType[] getComponentTypes() {
        return ComponentType.values();
    }

    public ExportType[] getExportTypes() {
        return ExportType.values();
    }

    public void removeSelectedComponent() {
        if (selectedComponent == null) return;
        NetworkSite site = SiteManager.getCurrentSite();
        site.getTopology().getComponents().remove(selectedComponent);
        siteService.update(site);
        componentList.remove(selectedComponent);
        selectedComponent = null;
    }

    public void editSelectedComponent() {
        if (selectedComponent == null) return;
        NetworkSite site = SiteManager.getCurrentSite();
        site.getTopology().getComponents().remove(selectedComponent);
        site.getTopology().getComponents().add(selectedComponent);
        siteService.update(site);
    }

    public void addSelectedComponent() {
        NetworkSite site = SiteManager.getCurrentSite();
        componentService.save(addComponent);
        site.getTopology().getComponents().add(addComponent);
        siteService.update(site);
        init();
    }

}
