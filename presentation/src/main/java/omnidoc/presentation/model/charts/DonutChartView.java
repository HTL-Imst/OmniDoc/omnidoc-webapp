package omnidoc.presentation.model.charts;

import omnidoc.business.domain.site.NetworkSite;
import omnidoc.business.domain.topology.ComponentType;
import omnidoc.business.domain.topology.NetworkComponent;
import omnidoc.business.services.site.SiteService;
import omnidoc.presentation.manager.SiteManager;
import omnidoc.presentation.util.Resources;
import org.primefaces.model.chart.DonutChartModel;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

@Named
@ViewScoped
public class DonutChartView implements Serializable {

    private static final long serialVersionUID = -5816176710866257767L;

    private DonutChartModel donutModel;

    @Inject
    private SiteService siteService;

    @PostConstruct
    public void init() {
        donutModel = createDonutModel();
    }

    public DonutChartModel getDonutModel() {
        return donutModel;
    }

    public void setDonutModel(DonutChartModel donutModel) {
        this.donutModel = donutModel;
    }

    private DonutChartModel createDonutModel() {
        DonutChartModel model = initDonutModel();
        ResourceBundle bundle = Resources.getMessageBundle(Resources.dashboardBundle);

        model.setTitle(bundle.getString("panel.chart.components"));
        model.setLegendPosition("w");

        model.setShowDataLabels(true);
        model.setDataFormat("value");
        return model;
    }

    private DonutChartModel initDonutModel() {
        DonutChartModel model = new DonutChartModel();

        NetworkSite site = siteService.findById(SiteManager.getCurrentSite().getId());
        if (site.getTopology() == null) return model;

        Set<NetworkComponent> components = site.getTopology().getComponents();
        Map<String, Number> componentCircle = new LinkedHashMap<>();
        for (ComponentType componentType : ComponentType.values()) {
            componentCircle.put(
                    componentType.name(),
                    components.stream()
                            .filter(c -> c.getType() == componentType)
                            .count());
        }
        model.addCircle(componentCircle);

        return model;
    }
}
