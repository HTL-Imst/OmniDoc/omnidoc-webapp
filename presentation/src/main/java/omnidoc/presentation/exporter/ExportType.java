package omnidoc.presentation.exporter;

public enum ExportType {
    EXCEL("xls"), CSV("csv"), XML("xml");

    private String type;

    ExportType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

}
