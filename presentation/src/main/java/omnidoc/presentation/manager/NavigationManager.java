package omnidoc.presentation.manager;

import omnidoc.presentation.util.Page;

public class NavigationManager {

    private static final String redirectionParam = "?faces-redirect=true";
    private static final String PARAM_DETAIL = "page";

    public static String to(Page page) {
        String url = "/" + page.getName() + ".xhtml" + redirectionParam;
        switch (page) {
            case SIMPLE_DIAGRAM:
            case EDITABLE_DIAGRAM:
            case MINDMAP:
            case ORGANIGRAM:
                return to(Page.DETAIL) + "&" + PARAM_DETAIL + "=" + page.getName();
        }
        return url;
    }
}
