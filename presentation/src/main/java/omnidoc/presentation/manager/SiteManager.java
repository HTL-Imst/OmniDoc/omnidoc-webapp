package omnidoc.presentation.manager;

import omnidoc.business.domain.site.NetworkSite;

public class SiteManager {

    private static NetworkSite currentSite;

    public static NetworkSite getCurrentSite() {
        return currentSite;
    }

    public static void setCurrentSite(NetworkSite currentSite) {
        SiteManager.currentSite = currentSite;
        checkSiteSelected();
    }

    public static void checkSiteSelected() {
        if (currentSite == null) {
            MenuManager.disableSiteMenuEntries();
        } else {
            MenuManager.enableSiteMenuEntries();
        }
    }

    public static boolean isSelected(NetworkSite site) {
        return currentSite != null && currentSite.equals(site);
    }
}
