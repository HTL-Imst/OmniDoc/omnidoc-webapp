package omnidoc.presentation.converter;

import omnidoc.business.domain.scan.NetworkScan;
import omnidoc.business.services.scans.ScanService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

@Named
public class NetworkScanSelectConverter implements Converter {

    @Inject
    private ScanService scanService;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String scanId) {
        if (scanId == null || scanId.isEmpty()) return null;
        return scanService.findById(Long.parseLong(scanId));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return value == null ? null : ((NetworkScan) value).getId().toString();
    }
}
