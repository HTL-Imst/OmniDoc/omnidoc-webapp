package omnidoc.presentation.data;

import omnidoc.business.domain.topology.ComponentType;
import omnidoc.business.domain.topology.NetworkComponent;
import omnidoc.business.domain.topology.NetworkConnection;
import omnidoc.business.domain.topology.NetworkTopology;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

public class TopologyProducer {

    public static NetworkTopology createTestTopology() {
        NetworkComponent router1 = new NetworkComponent(ComponentType.ROUTER, "192.168.0.1", "router1", "Huawei AR3260", "123456789", Collections.singleton("FF:FF:FF:FF"));
        NetworkComponent router2 = new NetworkComponent(ComponentType.ROUTER, "192.168.0.2", "router2", "Huawei AR2220", "123456789", Collections.singleton("FF:FF:FF:FF"));
        NetworkComponent switch1 = new NetworkComponent(ComponentType.SWITCH, "192.168.0.3", "switch1", "Cisco Catalyst 2950", "123456789", Collections.singleton("FF:FF:FF:FF"));
        NetworkComponent switch2 = new NetworkComponent(ComponentType.SWITCH, "192.168.0.4", "switch2", "Cisco Catalyst 2950", "123456789", Collections.singleton("FF:FF:FF:FF"));
        NetworkComponent client1 = new NetworkComponent(ComponentType.CLIENT, "192.168.0.5", "client1", "Computer A", "123456789", Collections.singleton("FF:FF:FF:FF"));
        NetworkComponent client2 = new NetworkComponent(ComponentType.CLIENT, "192.168.0.6", "client2", "Computer B", "123456789", Collections.singleton("FF:FF:FF:FF"));
        NetworkComponent client3 = new NetworkComponent(ComponentType.CLIENT, "192.168.0.7", "client3", "Computer C", "123456789", Collections.singleton("FF:FF:FF:FF"));

        NetworkConnection r1r2 = new NetworkConnection("192.168.0.1/24", router1, router2);
        NetworkConnection r1s1 = new NetworkConnection("192.168.0.0/24", router1, switch1);
        NetworkConnection r2r2 = new NetworkConnection("192.168.2.0/24", router2, switch2);
        NetworkConnection s1c1 = new NetworkConnection("", switch1, client1);
        NetworkConnection s1c2 = new NetworkConnection("", switch1, client2);
        NetworkConnection s2c3 = new NetworkConnection("", switch2, client3);

        return new NetworkTopology(
                new HashSet<>(Arrays.asList(router1, router2, switch1, switch2, client1, client2, client3)),
                new HashSet<>(Arrays.asList(r1r2, r1s1, r2r2, s1c1, s1c2, s2c3))
        );

    }

}
