package omnidoc.presentation.data;

import omnidoc.business.domain.property.snmp.*;
import omnidoc.business.domain.scan.*;
import omnidoc.business.domain.site.NetworkSite;
import omnidoc.business.services.scans.ScanService;
import omnidoc.business.services.settings.ISettingsService;
import omnidoc.business.services.snmp.ObjectIdentifierService;
import omnidoc.business.repositories.SiteRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Calendar;
import java.util.Date;

@Stateless
public class SiteProducer {

    @Inject
    private SiteRepository siteRepository;

    @Inject
    private ISettingsService settingsService;

    @Inject
    private ScanService scanService;

    @Inject
    private ObjectIdentifierService oidService;

    public NetworkSite createTestSite(String siteName) {
        NetworkScan scan1 = new NetworkScan("Primary Subnet", createCommunityScan(SNMP_VERSION.VERSION_1), ScheduleType.MANUAL);
        NetworkScan scan2 = new ScheduledNetworkScan("Secondary Subnet", createCommunityScan(SNMP_VERSION.VERSION_2), new Date());
        NetworkScan scan3 = new FrequentNetworkScan("Ternary Subnet", createCredentialScan(HASH_ALGORITHM.SHA), new Date(), ScheduleType.MANUAL);

        scan1.setResult(TopologyProducer.createTestTopology());
        scan2.setResult(TopologyProducer.createTestTopology());
        scan2.setResult(TopologyProducer.createTestTopology());

        NetworkSite site = new NetworkSite(siteName);
        site.setTopology(TopologyProducer.createTestTopology());
        site.addScan(scan1);
        site.addScan(scan2);
        site.addScan(scan3);
        return site;
    }

    private SnmpProperty createCommunityScan(SNMP_VERSION version) {
        return new SnmpProperty(
                "10.0.0.0",
                24,
                oidService.getDefaultObjectIdentifier(),
                version,
                "public"
        );
    }

    private SnmpCredentialProperty createCredentialScan(HASH_ALGORITHM hashAlgorithm, ENCRYPTION_ALGORITHM encryptionAlgorithm, String privateKey) {
        return new SnmpCredentialProperty(
                "10.0.0.0",
                24,
                oidService.getDefaultObjectIdentifier(),
                "public",
                "user",
                "password",
                hashAlgorithm,
                encryptionAlgorithm,
                privateKey
        );
    }

    private SnmpCredentialProperty createCredentialScan(HASH_ALGORITHM hashAlgorithm) {
        return new SnmpCredentialProperty(
                "10.0.0.0",
                24,
                oidService.getDefaultObjectIdentifier(),
                "public",
                "user",
                "password",
                hashAlgorithm
        );
    }


    private SnmpCredentialProperty createCredentialScan() {
        return new SnmpCredentialProperty(
                "10.0.0.0",
                24,
                oidService.getDefaultObjectIdentifier(),
                "public",
                "user",
                "password"
        );
    }

    public void createExampleSite() {
        NetworkSite site = createTestSite("Example Site");
        siteRepository.save(site, true);
        settingsService.addPreferences(site, SettingsProducer.createExampleSiteDashboardPreference());
    }

    public void createManualTestScans() {
        NetworkSite site = new NetworkSite("Manual Site");

        NetworkScan scan1 = new NetworkScan("Manual Scan 1", createCommunityScan(SNMP_VERSION.VERSION_1), ScheduleType.MANUAL);
        NetworkScan scan2 = new NetworkScan("Manual Scan 2", createCommunityScan(SNMP_VERSION.VERSION_2), ScheduleType.MANUAL);
        NetworkScan scan3 = new NetworkScan("Manual Scan 3", createCredentialScan(HASH_ALGORITHM.MD5), ScheduleType.MANUAL);
        scan1.setSite(site);
        scan2.setSite(site);
        scan3.setSite(site);

        site.addScan(scan1);
        site.addScan(scan2);
        site.addScan(scan3);

        siteRepository.save(site, true);
        scanService.add(scan1);
        scanService.add(scan2);
        scanService.add(scan3);
    }

    public void createSiteForPresentation() {
        NetworkSite site = new NetworkSite("HTL-IMST");

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();

        NetworkScan scan1 = new NetworkScan("Scan 1", createCredentialScan(HASH_ALGORITHM.SHA, ENCRYPTION_ALGORITHM.DES, "key"), ScheduleType.MANUAL);
        NetworkScan scan2 = new ScheduledNetworkScan("Scan 2", createCommunityScan(SNMP_VERSION.VERSION_2), tomorrow);
        NetworkScan scan3 = new FrequentNetworkScan("Scan 3", createCredentialScan(HASH_ALGORITHM.SHA), tomorrow, ScheduleType.DAILY);

        scan3.setState(ScanState.FINISHED);
        scan3.setResult(TopologyProducer.createTestTopology());
        site.setTopology(TopologyProducer.createTestTopology());

        scan1.setSite(site);
        scan2.setSite(site);
        scan3.setSite(site);

        site.addScan(scan1);
        site.addScan(scan2);
        site.addScan(scan3);
        site.addScan(scan3);

        siteRepository.save(site, true);
        scanService.add(scan1);
        scanService.add(scan2);
        scanService.add(scan3);
        scanService.add(scan3);
    }

    public void createSiteForPresentationFinal() {
        NetworkSite site = new NetworkSite("HTL-IMST");

        SnmpProperty property = new SnmpProperty(
                "10.0.10.0",
                29,
                oidService.getDefaultObjectIdentifier(),
                SNMP_VERSION.VERSION_1,
                "public"
        );

        NetworkScan scan1 = new NetworkScan("Scan 1", property, ScheduleType.MANUAL);

        scan1.setSite(site);
        site.addScan(scan1);

        siteRepository.save(site, true);
        scanService.add(scan1);
    }

}
