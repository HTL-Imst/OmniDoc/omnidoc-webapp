package omnidoc.presentation.data;

import omnidoc.presentation.model.panel.PanelType;

import java.util.ArrayList;
import java.util.List;

public class SettingsProducer {

    public static List<String>  createExampleSiteDashboardPreference() {
        List<String> panelNames = new ArrayList<>();

        panelNames.add(PanelType.COMPONENTS.getName());
        panelNames.add(PanelType.HELLO.getName());
        panelNames.add(PanelType.SIMPLE_DIAGRAM.getName());
        panelNames.add(PanelType.MINDMAP.getName());
        panelNames.add(PanelType.ORGANIGRAM.getName());
//        panels.add(PanelProducer.fromType(PanelType.EDITABLE_DIAGRAM));

        return panelNames;
    }

}
