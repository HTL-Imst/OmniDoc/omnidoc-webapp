package omnidoc.presentation.util;

import omnidoc.business.domain.scan.ScheduleType;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;

public class PropertyStringFormatter {

    public static String fromScheduleType(ScheduleType type, Date date) {
        ResourceBundle bundle = Resources.getMessageBundle(Resources.scanBundle);
        String msg = bundle.getString("scheduleType." + type.name() + ".desc");
        SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        String[] parameters = new String[3];
        if (date != null) {
            switch (type) {
                case ONCE:
                    parameters[0] = dateFormat.format(date);
                    break;
                case DAILY:
                    parameters[0] = hourFormat.format(date);
                    break;
                case WEEKLY:
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    parameters[0] = bundle.getString("day." + calendar.get(Calendar.DAY_OF_WEEK));
                    parameters[1] = hourFormat.format(date);
                    break;
                case CUSTOM:
                    break;
            }
        }
        return MessageFormat.format(msg, parameters);
    }

}
