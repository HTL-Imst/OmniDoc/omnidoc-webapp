package omnidoc.presentation.util.validation;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

public class JSFValidationHelper<T> {

    private FacesContext context;
    private Validator validator;

    public JSFValidationHelper(FacesContext context, Validator validator) {
        this.context = context;
        this.validator = validator;
    }

    public boolean validate(T t) {
        boolean valid = true;
        for (ConstraintViolation<T> constraintViolation : validator.validate(t)) {
            valid = false;
            FacesMessage msg = new FacesMessage(constraintViolation.getMessage());
            context.addMessage(null, msg);
        }
        return valid;
    }
}
