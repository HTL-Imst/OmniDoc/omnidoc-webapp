$(document).ready(function () {
    setupDashboard();
});

let svgElements = [];

/**
 * Setup dashboard
 */
function setupDashboard() {
    let diagramContent = $('#board').find('[id$="-content"]');
    Array.from(diagramContent).forEach(element => {
        if (element.classList.contains('ui-mindmap')) {
            setupMindmap(element);
        }
    });

    setupPanelDragging();
}

function setupPanelDragging() {
    $('.dragme').draggable({
        helper: 'clone',
        scope: 'dashboard',
        zIndex: ++PrimeFaces.zindex
    });

    $('#pnlDashboard').droppable({
        activeClass: 'ui-state-active',
        hoverClass: 'ui-state-highlight',
        tolerance: 'pointer',
        scope: 'dashboard',
        drop: function (event, ui) {
            let panelType = ui.draggable.find('.dragpanel-data')[0].innerHTML;
            onPanelDrop([
                {name: 'panelType', value: panelType}
            ]);
        }
    });
}

/**
 * Setup mindmap panel
 * @param mindmap
 */
function setupMindmap(mindmap) {

    // Provide Scrollbars to Mindmap content
    mindmap.style.position = "relative";
    mindmap.style.overflow = "auto";

    // Register callbacks
    mindmap.onmouseenter = function () {
        updateSvgDimensions();
    };
    mindmap.onmouseleave = function () {
        updateSvgDimensions();
    };

    // Provide ID for mindmap svg mindmap
    Array.prototype.forEach.call(mindmap.children, function (child) {
        if (child instanceof SVGElement) {
            svgElements.push(child);
        }
    });

    updateSvgDimensions();
    registerSvgCallbacks();
}

/**
 * Register callbacks on svg child elements
 */
function registerSvgCallbacks() {
    Array.prototype.forEach.call(svgElements, function (svg) {
        Array.prototype.forEach.call(svg.children, function (svgPath) {
            svgPath.onmousedown = function () {
                this.onmousemove = function () {
                    updateSvgDimensions();
                }
            };
            svgPath.onmouseup = function () {
                this.onmousemove = null;
                updateSvgDimensions();
            };
            svgPath.onmouseleave = function () {
                updateSvgDimensions();
            };
        });
    });
}

/**
 * Unregister callbacks on svg child elements
 */
function unregisterSvgCallbacks() {
    Array.prototype.forEach.call(svgElements, function (svg) {
        Array.prototype.forEach.call(svg.children, function (svgPath) {
            svgPath.onmousedown = null;
            svgPath.onmousemove = null;
            svgPath.onmouseup = null;
            svgPath.onmouseleave = null;
        });
    });
}

/**
 * Make diagram scale along panel resizing in dashboard
 * @param event
 * @param ui
 */
function handleResize(event, ui) {
    let panelHeader = ui.element.find('[id$="_header"]');
    let diagramContent = ui.element.find('[id$="-content"]');
    diagramContent.width(ui.size.width);
    diagramContent.height(ui.size.height - panelHeader.outerHeight());

    updateSvgDimensions();
}

function handlePanelClose() {
    svgElements = [];
    unregisterSvgCallbacks()
}

/**
 * AJAX mindmap select callback
 */
function mindmapSelectCallback() {
    setTimeout(function () {
        updateSvgDimensions();
        registerSvgCallbacks();
    }, 500);
}

/**
 * Resize SVG dimensions to fit all child elements
 */
function updateSvgDimensions() {
    try {
        for (let i = 0; i < svgElements.length; i++) {
            let svg = svgElements[i];
            svg.setAttribute('width', svg.getBBox().x + svg.getBBox().width);
            svg.setAttribute('height', svg.getBBox().y + svg.getBBox().height);
        }
    } catch (ignored) {
    }
}