$(document).ready(function () {
    drawBackgroundImage();
});

/**
 * AJAX callback when adding site
 * @param xhr
 * @param status
 * @param args
 */
function handleAddSiteRequest(xhr, status, args) {
    if (args.success) {
        PF('js-dlg-addsite').hide();
    } else {
        PF('js-dlg-addsite').jq.effect("shake", {times: 5}, 100);
    }
}

/**
 * Draws Full HD Wallpaper from unsplash onto startpage
 */
function drawBackgroundImage() {
    let content = document.getElementById("content");
    if (content.style.backgroundImage === "") {
        content.style.backgroundImage = "url('http://source.unsplash.com/random/1920x1800')";
        content.style.backgroundSize = "cover";
    }
}


