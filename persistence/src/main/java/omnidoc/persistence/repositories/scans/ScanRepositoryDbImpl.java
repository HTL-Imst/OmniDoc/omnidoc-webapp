package omnidoc.persistence.repositories.scans;

import omnidoc.business.domain.scan.NetworkScan;
import omnidoc.business.domain.site.NetworkSite;
import omnidoc.business.repositories.ScanRepository;
import omnidoc.business.repositories.SiteRepository;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class ScanRepositoryDbImpl implements ScanRepository {

    @Inject
    private SiteRepository siteRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<NetworkScan> findAllBySiteId(Long siteId) {
        TypedQuery<NetworkScan> query = entityManager.createNamedQuery(NetworkScan.findBySiteId, NetworkScan.class);
        NetworkSite site = siteRepository.findById(siteId);
        query.setParameter("site", site);
        return query.getResultList();
    }

    public NetworkScan findById(Long scanId) {
        return entityManager.find(NetworkScan.class, scanId);
    }

    @Override
    public NetworkScan save(NetworkScan scan, boolean immediate) {
        entityManager.persist(scan);
        if (immediate) entityManager.flush();
        return scan;
    }

    @Override
    public void update(NetworkScan scan) {
        entityManager.merge(scan);
    }

    @Override
    public void remove(NetworkScan scan) {
        scan = entityManager.contains(scan) ? scan : entityManager.merge(scan);
        scan.getSite().getScans().remove(scan);
        scan.setSite(null);
        entityManager.remove(scan);
    }

}
