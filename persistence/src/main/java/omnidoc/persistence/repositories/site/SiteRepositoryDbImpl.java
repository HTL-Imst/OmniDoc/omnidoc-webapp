package omnidoc.persistence.repositories.site;

import omnidoc.business.domain.site.NetworkSite;
import omnidoc.business.repositories.SiteRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class SiteRepositoryDbImpl implements SiteRepository {

    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public List<NetworkSite> findAll() {
        TypedQuery<NetworkSite> query = entityManager.createNamedQuery(NetworkSite.findAll, NetworkSite.class);
        return query.getResultList();
    }

    @Override
    public NetworkSite findById(Long siteId) {
        return entityManager.find(NetworkSite.class, siteId);
    }

    @Override
    public NetworkSite save(NetworkSite site, boolean immediate) {
        entityManager.persist(site);
        if (immediate) entityManager.flush();
        return site;
    }

    @Override
    public void update(NetworkSite site) {
        entityManager.merge(site);
    }

    @Override
    public void remove(NetworkSite site) {
        entityManager.remove(entityManager.merge(site));
    }

}
