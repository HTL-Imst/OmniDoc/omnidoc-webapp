package omnidoc.persistence.repositories.site;

import omnidoc.business.domain.site.NetworkSite;
import omnidoc.business.repositories.SiteRepository;

import javax.enterprise.inject.Alternative;
import java.util.ArrayList;
import java.util.List;

@Alternative
public class SiteRepositoryImpl implements SiteRepository {

    private List<NetworkSite> sites = new ArrayList<>();

    @Override
    public List<NetworkSite> findAll() {
        return sites;
    }

    @Override
    public NetworkSite findById(Long siteId) {
        for (NetworkSite site : sites) {
            if (site.getId().equals(siteId)) return site;
        }
        return null;
    }

    @Override
    public NetworkSite save(NetworkSite site, boolean immediate) {
        sites.add(site);
        return site;
    }

    @Override
    public void update(NetworkSite site) {
        for (int i = 0; i < sites.size(); i++) {
            if (sites.get(i).getId().equals(site.getId())) {
                sites.set(i, site);
            }
        }
    }

    @Override
    public void remove(NetworkSite site) {
        sites.remove(site);
    }

}
