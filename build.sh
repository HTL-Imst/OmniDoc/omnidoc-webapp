#!/bin/sh

mvn clean package \
    && docker-compose up -d --build \
    && docker-compose logs -f
